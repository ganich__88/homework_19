#include <iostream>

class Animal
{
public:
    Animal() {}
    virtual ~Animal() {}
    virtual void Voice() {}
};

class Cat : public Animal
{
public:
    Cat() {}
    ~Cat() {}
    void Voice() override
    {
        std::cout << "Mew - Mew\n";
    }
};

class Dog : public Animal
{
public:
    Dog() {}
    ~Dog() {}
    void Voice() override
    {
        std::cout << "Woof\n";
    }
};

class Pig : public Animal
{
public:
    Pig() {}
    ~Pig() {}
    void Voice() override
    {
        std::cout << "Oink\n";
    }
};
int main()
{ 
    
    Cat* C = new Cat;
    Dog* D = new Dog;
    Pig* P = new Pig;

    Animal** Array = new Animal * [3] { C, D, P};

    for (int i = 0; i < 3; i++)
    {
        Array[i]->Voice();
        delete Array[i];
    }   
    //delete Array[0];
    //delete Array[1];
    //delete Array[2];
    return 0;
}